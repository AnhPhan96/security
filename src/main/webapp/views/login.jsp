<%--
  Created by IntelliJ IDEA.
  User: Anh
  Date: 11/2/2017
  Time: 7:18 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Login</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href='<c:url value="/css/style.css"/>'>
    <script type="text/javascript" src='<c:url value="/js/script.js"/>'></script>
</head>
<body>

<div class="container-fluid">
    <nav class="navbar navbar-inverse">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-4">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Brand</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar-collapse-4">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">Support</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> Account <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">My profile</a></li>
                            <li><a href="#">Favorited</a></li>
                            <li><a href="#">Settings</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container -->
    </nav><!-- /.navbar -->
</div><!-- /.container-fluid -->


<div class="container main" style="width: 288px">
    <div class="row">
        <div class="container" id="formContainer" style="width: 288px">

            <form action="login" method="post" class="form-signin" id="login" role="form">
                <h3 class="form-signin-heading">Please sign in</h3>
                <a href="#" id="flipToRecover" class="flipLink">
                    <div id="triangle-topright"></div>
                </a>
                <input type="text" class="form-control" name="username" placeholder="User name" required autofocus>
                <input type="password" class="form-control" name="password" id="loginPass" placeholder="Password" required>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            </form>

            <form action="/sample/register" method="post" class="form-signin" id="recover" role="form">
                <h3 class="form-signin-heading" style="margin-top: 20px">Register</h3>
                <a href="#" id="flipToLogin" class="flipLink">
                    <div id="triangle-topleft"></div>
                </a>
                <input type="text" class="form-control" name="userName" placeholder="User name" required autofocus/>
                <input type='password' name='passWord' class="form-control" placeholder="Password" required/>
                <input name='groupName' class="form-control" placeholder="Role" required/>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
            </form>

        </div> <!-- /container -->
    </div>
</div>
</body>
</html>
