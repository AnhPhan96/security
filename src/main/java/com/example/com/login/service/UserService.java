package com.example.com.login.service;

import com.example.com.login.entity.User;
import com.example.com.login.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public void saveCustomer(User user) {
        userRepository.save(user);
    }
}
