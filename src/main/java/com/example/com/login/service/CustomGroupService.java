package com.example.com.login.service;

import com.example.com.login.entity.CustomGroup;
import com.example.com.login.repository.CustomGroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomGroupService {

    @Autowired
    private CustomGroupRepository customGroupRepository;

    public CustomGroup findByName(String name) {
        return customGroupRepository.findByName(name);
    }
}
