package com.example.com.login.controller;

import com.example.com.login.entity.CurrentUser;
import com.example.com.login.entity.User;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ProfileController {

    @GetMapping("/profile")
    public ModelAndView profile(ModelAndView modelAndView ,Authentication model){
        CurrentUser user = (CurrentUser) model.getPrincipal();
        User us = user.getUser();
        System.out.println(us.getUsername());
        modelAndView.addObject("currentUser", us);
        modelAndView.setViewName("profile");
        return modelAndView;
    }
}
