package com.example.com.login.controller;

import com.example.com.login.entity.CustomGroup;
import com.example.com.login.entity.RegisterParam;
import com.example.com.login.entity.User;
import com.example.com.login.service.CustomGroupService;
import com.example.com.login.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class RegisterController {

    @Autowired
    private UserService userService;

    @Autowired
    private CustomGroupService customGroupService;

    @Autowired
    private PasswordEncoder encoder;

//    @GetMapping("/register")
//    public ModelAndView register(ModelAndView model){
//        return new ModelAndView("register", "user", new RegisterParam());
//    }

    @GetMapping("/register")
    public String register(){
        return "register";
    }

//    @PostMapping("/register")
////    @ResponseBody
//    public ResponseEntity<?> handleRegister(@ModelAttribute("user") RegisterParam registerParam){
//        String username = registerParam.getUserName();
//        String password = registerParam.getPassWord();
//        User user = new User(username, encoder.encode(password));
//        CustomGroup customGroup = customGroupService.findByName(registerParam.getGroupName());
//        user.setGroup(customGroup);
//        userService.saveCustomer(user);
//
//        return ResponseEntity.status(HttpStatus.CREATED).body(user);
//    }

    @PostMapping("/register")
//    @ResponseBody
    public RedirectView handleRegister(@ModelAttribute("user") RegisterParam registerParam){
        String username = registerParam.getUserName();
        String password = registerParam.getPassWord();
        User user = new User(username, encoder.encode(password));
        CustomGroup customGroup = customGroupService.findByName(registerParam.getGroupName());
        user.setGroup(customGroup);
        userService.saveCustomer(user);

        return new RedirectView("/sample/login");
    }
}
