package com.example.com.login.repository;

import com.example.com.login.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
    User findByUsernameOrPhone(String username, String phoneNumber);
    User findByUsername(String name);
}
