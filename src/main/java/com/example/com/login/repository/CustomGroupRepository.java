package com.example.com.login.repository;

import com.example.com.login.entity.CustomGroup;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomGroupRepository extends JpaRepository<CustomGroup, Integer> {
    public CustomGroup findByName(String name);
}
