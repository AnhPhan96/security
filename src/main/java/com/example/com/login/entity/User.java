package com.example.com.login.entity;

import javax.persistence.Id;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "user")
public class User implements Serializable {
    private Integer id;
    private String username;
    private String password;
    private String phone;
    private CustomGroup group;

    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    public User(){

    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(nullable = false, unique = true)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @ManyToOne
    @JoinColumn(name = "fk_group")
    public CustomGroup getGroup() {
        return group;
    }

    public void setGroup(CustomGroup group) {
        this.group = group;
    }
}
