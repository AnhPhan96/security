package com.example.com.login.entity;

import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;

public class CurrentUser extends User {

    private com.example.com.login.entity.User user;

    public CurrentUser(com.example.com.login.entity.User user) {
        super(user.getUsername(), user.getPassword(), AuthorityUtils.createAuthorityList(user.getGroup().getName()));
        this.user = user;
    }

    public Integer getId(){
        return user.getId();
    }

    public com.example.com.login.entity.User getUser() {
        return user;
    }

    public void setUser(com.example.com.login.entity.User user) {
        this.user = user;
    }
}
